package kz.astana.viewapp;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Calendar minDate = Calendar.getInstance();
        minDate.set(2019, 8, 30);

        Calendar maxDate = Calendar.getInstance();
        maxDate.set(2021, 2, 20);

        DatePicker dp = findViewById(R.id.datePicker);
        dp.setMinDate(minDate.getTimeInMillis());
        dp.setMaxDate(maxDate.getTimeInMillis());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dp.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Log.d("Hello", dayOfMonth + "." + monthOfYear + "." + year);
                }
            });
        }

        Button b = findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        b.setEnabled(false);
    }

    private void openDialog() {
        Calendar c = Calendar.getInstance();

        DatePickerDialog tpd =
                new DatePickerDialog(
                        SecondActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Log.d("Hello", dayOfMonth + "." + month + "." + year);
                            }
                        },
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)
                );
        tpd.show();
    }
}